document.addEventListener('DOMContentLoaded', function () {
    const container2 = document.querySelector(".container2");
    let emojis = ["🙂", "😍", "🥑", "🐱", "🌼", "🎉", "🍕", "🐧", "🙂", "😍", "🥑", "🐱", "🌼", "🎉", "🍕", "🐧"];

    emojis = shuffleArray(emojis);

    for (let i = 0; i < 16; i++) {
      const newElement = document.createElement('div');
      newElement.classList.add('inner-container');

      const card = document.createElement('div');
      card.classList.add('card');

      const front = document.createElement('div');
      front.classList.add('front');

      const back = document.createElement('div');
      back.classList.add('back');
      back.innerText = emojis[i % 16];

      card.appendChild(front);
      card.appendChild(back);
      newElement.appendChild(card);

      container2.appendChild(newElement);
    }
    
    const startButton = document.querySelector(".start-btn");
    let timerInterval;
    let startClicked = false
    startButton.addEventListener('click', function () {
      startButton.classList.add('clicked');
      timer = 0;
      moveCount = 0;
      startClicked = true;
      updateMoveCount();
      clearInterval(timerInterval); 
      updateTimerDisplay(); 
      resetCards(); 
      timerInterval = setInterval(function () {
        timer++;
        updateTimerDisplay();
      }, 1000);
    });

    const cards = document.querySelectorAll(".card");
    let flippedCards = [];
    let moveCount = 0; 
    let timer = 0; 
    let totalFlippedCards = 0;
    cards.forEach(card => {
      card.addEventListener('click', function () {
        if (flippedCards.length < 2 && !flippedCards.includes(card)) {
          card.style.transform = "rotateY(180deg)";
          flippedCards.push(card);
          if(startClicked){
              moveCount++; 
          }

          if (flippedCards.length === 2) {
            setTimeout(() => {
              const [card1, card2] = flippedCards;
              if (card1.querySelector('.back').innerText !== card2.querySelector('.back').innerText) {
                card1.style.transform = "rotateY(0deg)";
                card2.style.transform = "rotateY(0deg)";
                
              }
              if (card1.querySelector('.back').innerText === card2.querySelector('.back').innerText) {
                totalFlippedCards += 2;
              }
              flippedCards = [];
              updateMoveCount(); 
            }, 1000);
          }
          updateMoveCount(); 
        }
      });
    });

    function updateMoveCount() {
      const countMoves = document.querySelector(".count-moves");
      countMoves.textContent = moveCount; 
    }

    function resetCards() {
      cards.forEach(card => {
        card.style.transform = "rotateY(0deg)";
      });
    }

    function shuffleArray(array) {
        const shuffled = [...array];
        shuffled.sort(() => Math.random() - 0.5);
        return shuffled;
      }

    
    function updateTimerDisplay() {
      const countTime = document.querySelector(".count-time");
      const minutes = Math.floor(timer / 60);
      const seconds = timer % 60;
      if(totalFlippedCards < 16){
          countTime.textContent = `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
      }
    }
});